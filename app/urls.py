from django.conf.urls import url
from .views import *

#url for app
urlpatterns = [
    url(r'^job/listing/', show_jobs, name='job-listing'),
    url(r'^student/list/', show_student, name='student-list'),
    url(r'^job/forum/(?P<id>\d)/$', show_job_forum, name='job-forum'),
    url(r'^job/forum/(?P<id>\d)/post/$', add_comment, name='post-comment'),
    url(r'^job/comment/delete/(?P<id>\d)/$', delete_comment, name='delete-comment'),
	url(r'^student/profile/(?P<pk>.*)/$', show_student_profile, name='student-profile'),
    url(r'^company/logout/', auth_logout, name='company-logout'),
    url(r'^job/forum/(?P<id>\d)/apply/$', add_application, name='apply-job'),
]
