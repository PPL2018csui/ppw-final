from django.apps import AppConfig


class AppRenderConfig(AppConfig):
    name = 'app_render'
