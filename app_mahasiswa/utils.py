from django.core import serializers
from app_mahasiswa.models import User

def check_user_existence(**kwargs):
    '''
    Check if user has existed or not.
    :param npm: identity number
    :return: boolean True if user is exist, False otherwise
    '''
    if len(kwargs) == 0:
        return False

    return bool(User.objects.filter(**kwargs))


def get_or_create_user(npm, **kwargs):
    '''
    Get user by NPM if user has existed, otherwise create new one and return it.
    The **kwargs argument is only for create, for get only using npm.
    :param npm: identity number
    :return: User
    '''
    if check_user_existence(npm=npm):
        user = User.objects.filter(npm=npm)

    else:
        user = User(npm=npm)

        if kwargs:
            if kwargs['name']:
                user.name = kwargs['name']
            if kwargs['angkatan']:
                user.angkatan = kwargs['angkatan']
            if kwargs['role']:
                user.role = kwargs['role']

        user.save()

        user = [user]

    return user

def serialize_user(user):
  return serializers.serialize('json', user)