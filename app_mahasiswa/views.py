from django.shortcuts import render, reverse
from app_mahasiswa.models import User
from .helper.helper import RiwayatHelper
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect

response = {}
helper = RiwayatHelper()

def index(request):
	if not 'session-login' in request.session.keys():
		print(request.session.keys())
		html = 'login/login_page.html'
		return render(request, html, response)
		pass
	else:
		mahasiswa = User.objects.get(npm=request.session['npm'])
		response['mahasiswa'] = mahasiswa
		riwayat_list = helper.instance.get_riwayat_list()
		response['riwayat_list']=riwayat_list

	html = 'mahasiswa/dashboard.html'
	return render(request, html, response)

def add_status(request):
	#form = Status_Form(request.POST)
	#if(request.method == 'POST' and form.is_valid()):
	#	response['status'] = request.POST['status']
	#	status = Status(status=response['status'])
	#	status.save()
	#	return HttpResponseRedirect('/mahasiswa/dashboard/')
	#else:
	return HttpResponseRedirect('/mahasiswa/dashboard/')

def delete_status(request, pk):
	#status = Status.objects.filter(pk=pk).first()
	#if status != None:
	#	status.delete()
	#	pass
	return HttpResponseRedirect('/mahasiswa/dashboard/')
