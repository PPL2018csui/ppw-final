from django.db import models

class User(models.Model):
    # public
    npm = models.CharField('NPM', primary_key=True, max_length=10, editable=False, unique=True)
    name = models.CharField(max_length=50)

    # private
    role = models.CharField('Role', blank=True, max_length=10)
    angkatan = models.PositiveIntegerField('Angkatan')
    is_showing_score = models.BooleanField(default=False)

    # linkedin
    picture_url = models.CharField(blank=True, max_length=300)
    id_linkedin = models.CharField(blank=True, max_length=20)
    link_linkedin = models.CharField(blank=True, max_length=100)

    lastseen_at = models.DateTimeField('Last Seen at', auto_now=True, editable=False)
    created_at = models.DateTimeField('Created at', auto_now_add=True, editable=False)

    class Meta:
        ordering = ('npm', 'name', 'created_at', 'lastseen_at')
