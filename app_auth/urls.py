from django.conf.urls import url
from .views import *

#url for app
urlpatterns = [
    url(r'^company/login/', request_auth, name='company-login'),
    url(r'^company/dummy/login/', dummy_auth, name='dummy-login'),
    url(r'^company/token/', request_token , name='company-request-token'),
    url(r'^company/logout/', auth_logout, name='company-logout'),
    url(r'^user/login/', auth_login, name='user-login'),
    url(r'^user/linkedin/login/', request_auth_user, name='user-request-auth'),
    url(r'^user/linkedin/token/', request_token_user , name='user-request-token'),
    url(r'^user/logout/', auth_logout, name='user-logout'),
    url(r'^$', login_page, name='login'),
]
